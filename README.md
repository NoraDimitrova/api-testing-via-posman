# API Testing via POSTMAN

## API platforms

1. [REGRES_IN](https://reqres.in/)
2. [TRELLO](https://developer.atlassian.com/cloud/trello/rest/api-group-actions/)
3. [JIRA](https://docs.atlassian.com/software/jira/docs/api/REST/8.16.2/)

## Installation

1. Clone repo on your device
2. Download collection and environment and after that import it in POSTMAN

